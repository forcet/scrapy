# Instalación Scrapy y Beautifulsoup Windows#

Primero tenemos que instalar [python](https://www.python.org/) descargando un versión estable la cual puede ser [windows x86-64 msi installer](https://www.python.org/ftp/python/2.7.7/python-2.7.7.amd64.msi) ya descargado procedemos a instalar y una vez instalado ya tenemos corriedno python.

Ahora lo que debemos hacer es poner las variables de entorno: C:\Python27 y C:\Python27\Scripts;

Adicionalmente podemos instalar [pip install](https://raw.github.com/pypa/pip/master/contrib/get-pip.py) y [easy_install](https://pypi.python.org/packages/source/d/distribute/distribute-0.6.49.tar.gz)



### Perrequisitos ###

* Tener instalado python
* Descargar e instalar Visual C + + 2008 redistribuibles del OpenSSL Win32
* Descargar OpenSSL para windows
* Descargamos e instalamos twisted
* Descargamos e instalamos lxml


### Instalación de Scrapy ###

Como ya tenemos instalado el pip install o easy_install, en la consola procedemos a escribir lo siguiente: **easy_install Scrapy** y si es necesario instalamos el siguente modulo con: **pip install service_identity**