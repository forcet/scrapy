# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__="Mario"
__date__ ="$22/06/2014 03:06:49 PM$"


from bs4 import BeautifulSoup
from urllib2 import urlopen

url_cursos = "http://ocw.mit.edu"
base_url = "http://ocw.mit.edu/courses"

def cursos():
    lect = []
    html=soup(base_url)
    datos = html.find("div", { "id" : "course_wrapper" })
    for a in datos.find_all('a'):
        menu_enlace = a.get("href")
        if menu_enlace.startswith("#"):
            if menu_enlace != "#top" and menu_enlace!="#resources":
                nombre = menu_enlace.split("#")
                lect.append(nombre[1].encode('ascii', 'ignore'))
    return (lect)

def soup(url):
    response = urlopen(url)
    return BeautifulSoup(response.read())

def link_course(curso):
    #cont = 0
    lect = []
    curso_url   = '/'.join([base_url, curso])
    html        = soup(curso_url)    
    table       = html.find("table", { "class" : "courseList" })
    filas       = table.findAll("tr")
    for fila in filas:
        celdas = fila.findAll("td")
        if len(celdas) == 3:
            for link in celdas[0].find_all('a'):
                dir = ''.join([url_cursos, link.get('href')]) 
                lect.append(dir.encode('ascii', 'ignore'))
                
    return lect

def datos_course(curso):
    link = link_course(curso)
    len(link)
    cont = 0
    lect = []
    for cursos in range(len(link)):   
        html = soup(link[cursos])
        tit=html.title.getText().encode('utf-8')
        titulo = tit.split('|')
        div = html.find("div", { "id" : "course_info" }) 
        depart = html.find("div", { "id" : "breadcrumb_chp" }) 
        p       = div.findAll("p")
        pp = div.findAll("p", { "class" : "ins" }) 
        posi= len(pp)
        dep = depart.findAll('a')
        
        if cont >= cont:
            lect.append([])            
            lect[cont].append(":MIT_Curso_"+str(cursos+1))
            lect[cont].append(":hasDeparment")
            lect[cont].append(dep[2].getText())
            
            lect[cont].append("\n:MIT_Curso_"+str(cursos+1))
            lect[cont].append(":hastitle")
            lect[cont].append(titulo[0])
            
            lect[cont].append("\n:MIT_Curso_"+str(cursos+1))
            lect[cont].append(":hasId")
            lect[cont].append(p[posi].getText())
            
            lect[cont].append("\n:MIT_Curso_"+str(cursos+1))
            lect[cont].append(":hasUrl")
            lect[cont].append(link[cursos])
            
            lect[cont].append("\n:MIT_Curso_"+str(cursos+1))
            lect[cont].append(":hasLevel")
            lect[cont].append(p[posi+2].getText())
            
            posicion = range(posi)  
            #print posicion
            i = 1
            if posi > 1:
                for pext in posicion:
                    lect[cont].append("\n:MIT_Curso_"+str(cursos+1))
                    lect[cont].append(":hasProfessor")
                    lect[cont].append(":Professor_"+str(cursos+1)+"_"+str(i))

                    lect[cont].append("\n:Professor_"+str(cursos+1)+"_"+str(i))
                    lect[cont].append(":hasName")
                    lect[cont].append(pp[pext].getText())
                    i=i+1
            else:
                lect[cont].append("\n:MIT_Curso_"+str(cursos+1))
                lect[cont].append(":hasProfessor")
                lect[cont].append(":Professor_"+str(cursos+1))
            
                lect[cont].append("\n:Professor_"+str(cursos+1))
                lect[cont].append(":hasName")
                lect[cont].append(p[0].getText())
                        
            lect[cont].append("\n:MIT_Curso_"+str(cursos+1))
            lect[cont].append(":hasMenu")
            lect[cont].append(":Menu_"+str(cont+1))
            
            data = detalle_course(link[cursos])
            for dat in range(len(data[0])):
                
                lect[cont].append("\n:Menu_"+str(cont+1))
                lect[cont].append(":hasNameMenu")
                lect[cont].append(data[2][dat])
                
                lect[cont].append("\n:Menu_"+str(cont+1))
                lect[cont].append(":hasOER")
                lect[cont].append(":Resource_"+str(cursos+1)+"_"+str(dat+1))
                
                lect[cont].append("\n:Resource_"+str(cursos+1)+"_"+str(dat+1))
                lect[cont].append(":hasNameResource")
                lect[cont].append(data[0][dat])
                                
                lect[cont].append("\n:Resource_"+str(cursos+1)+"_"+str(dat+1))
                lect[cont].append(":hasUrl")
                lect[cont].append(data[1][dat])
                
            guardar(lect)
        cont = cont +1 
    return lect

def detalle_course(curso):
    html = soup(curso)
    current_link = ''
    lect=[]
    menu = []
    cont = 0
    nommenu = []
    pdf = []
    div       = html.find("div", { "id" : "course_nav" })
    for ul in div.find_all('ul'):
        for li in ul.find_all('li'):
            for a in li.find_all('a'):
                menu_enlace = a.get("href")
                if not(menu_enlace is None):
                    curso_url   = ''.join([url_cursos, menu_enlace])
                    menu.append(curso_url)
                    datos=soup(curso_url)
                    tit=datos.title.getText()
                    nombre = tit.split('|') 
                    for link in datos.find_all('a'):
                        current_link = link.get('href')
                        if not(current_link is None):
                           if current_link.endswith('pdf'):
                              current_link.encode('ascii', 'ignore')
                              url_pdf = ''.join([url_cursos, current_link])
                              lect.append(link.getText().encode('ascii', 'ignore'))
                              nommenu.append(nombre[0].encode('ascii', 'ignore'))
                              pdf.append(url_pdf)
                        cont = cont+1

    return lect, pdf, nommenu

def guardar(data):
    f = open('datosReadings.csv', 'a')
    lista = range(len(data))
    for i in lista:
        datos = ";".join(str(j.encode('ascii', 'ignore')) for j in data[i])       
    print (datos)
    f.write(datos+"\n")
        
    

    f.close()
    return "Datos Guardados"

if __name__ == '__main__':
    nombres = cursos()
    lista = range(len(nombres))
    for i in lista:
        nombre = nombres[i]
        data = datos_course(nombre.encode('ascii', 'ignore'))
#    data = datos_course("concourse")

        